using UnityEngine;

public class GameManager : MonoBehaviour
{
    public Joueur joueur;

    public float delaiReaparition = 3.0f;

    public int lives = 2;

    public void JoueurDeath()
    {
        this.lives--;

        if (this.lives == 0) {
            GameOver();
        } else {
            Invoke(nameof(Reaparition), this.delaiReaparition);
        }

        Invoke(nameof(Reaparition), this.delaiReaparition);
    }
    private void Reaparition()
    {
        this.joueur.transform.position = Vector3.zero;
        this.joueur.gameObject.SetActive(true);
    }

    private void GameOver()
    {
        this.lives = 2;

        Invoke(nameof(Reaparition), this.delaiReaparition);
    }   
}


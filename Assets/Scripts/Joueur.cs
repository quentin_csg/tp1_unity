using UnityEngine;

public class Joueur : MonoBehaviour
{
    public float defaultSpeed = 1.0f;
    public float defaultTurnSpeed = 1.0f;
    private Rigidbody2D _rigidbody;    
    private bool _verifyingDirection;

    private float _turnDirection;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
    }
    private void Update()
    {
        _verifyingDirection = Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.Z);

        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.Q)) {
            _turnDirection = 1.0f;
        } else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) {
            _turnDirection = -1.0f;
        } else {
            _turnDirection = 0.0f;
        }
    }

    private void FixedUpdate()
    {
        if (_verifyingDirection) {
            _rigidbody.AddForce(this.transform.up * this.defaultSpeed);
        }

        if (_turnDirection != 0.0f) {
            _rigidbody.AddTorque(_turnDirection * this.defaultTurnSpeed);            
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Asteroid")
        {
            _rigidbody.velocity = Vector3.zero;
            _rigidbody.angularVelocity = 0.0f;

            this.gameObject.SetActive(false);
            FindObjectOfType<GameManager>().JoueurDeath();
        }
    }
}

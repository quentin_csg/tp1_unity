using UnityEngine;

public class GenerateurAsteroid : MonoBehaviour
{
    public Asteroid PrefabAsteroid;
    
    public float spawnRate = 2.0f;

    public int spawnAmount = 1;

    public float distanceSpawn = 15.0f;

    public float variationTrajectoire = 15.0f;

    private void Start()
    {
        InvokeRepeating(nameof(Spawn), this.spawnRate, this.spawnRate);         
    }

    private void Spawn()
    {
        for (int i =0; i < this.spawnAmount; i++)
        {
            Vector3 spawnDirection = Random.insideUnitCircle.normalized * distanceSpawn;

            Vector3 spawnPoint = this.transform.position = spawnDirection;

            float variation = Random.Range(-this.variationTrajectoire, this.variationTrajectoire);
            Quaternion rotation = Quaternion.AngleAxis(variation, Vector3.forward);

            Asteroid asteroid = Instantiate(this.PrefabAsteroid, spawnPoint, rotation);
            asteroid.size = Random.Range(asteroid.minSize, asteroid.maxSize);
            asteroid.Trajectoire(rotation * -spawnDirection);
        }
    }
}
